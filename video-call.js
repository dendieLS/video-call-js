function getStream(config) {
  return new Promise((resolve, reject) => {
    navigator.mediaDevices.getUserMedia({
      audio: config.audio,
      video: config.video
    }).then((stream) => {
      console.log('stream')
      if (stream.getTracks()[0].kind === 'audio') {
        resolve(stream)
      } else {
        reject(new Error('MediaStream get video element first that can caused SDP Error'))
      }
    }).catch((err) => {
      throw new Error(err)
    })
  })
}